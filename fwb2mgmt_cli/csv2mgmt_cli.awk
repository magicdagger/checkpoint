# Take a CSV created by xml2csv.awk and generate mgmt_cli commands
# fab@clacks.xyz

{
    if ( NR == 1)
        next
    #id|type|name|comment|address|netmask|first;last|members
    split($0,ln,"|")
    id=ln[1]
    obj[id]=ln[2]
    names[id]=ln[3]
    comments[id]=ln[4]
    addresses[id]=ln[5]
    netmasks[id]=ln[6]
    ranges[id]=ln[7]
    members[id]=ln[8]
}

END {
    for ( id in obj) {
        printf("mgmt_cli add %s name \"%s\"",
            obj[id],
            names[id])

        if ( obj[id] == "host" ) {
            printf(" ip-address %s",
                addresses[id])
        } else if ( obj[id] == "network" ) {
            printf(" subnet %s subnet-mask %s",
                addresses[id],
                netmasks[id])
        } else if ( obj[id] == "address-range" ) {
            split(ranges[id],a,";")
            printf(" ip-address-first \"%s\" ip-address-last \"%s\"",
                a[1],
                a[2])
        } else if ( obj[id] == "group" ) {
            split(members[id],a,";")
            for (j in a)
                printf(" members.%d \"%s\"",j,names[a[j]])
        } else {
            printf ("Unhandled exception: %s\n",id) > "/dev/stderr"
            exit 255
        }

        if ( comments[id] )
            printf(" comments \"%s\"",comments[id])
        printf("\n")
    }
}
