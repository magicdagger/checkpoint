# Convert FW Builder config to CSV
# fab@clacks.xyz

function parse(ln,type,parent) {
    id=getPVal(ln,"id")
    name=getPVal(ln,"name")
    comment=getPVal(ln,"comment")
    if ( name ~ /\|/ || comment ~ /\|/ )
        printf("%s contains '|'\n",id) > "/dev/stderr"
    if ( type == "group") {
        lastgroup=id
        groups[id]="group"
        names[id]=name
        comments[id]=comment
        return
    } else if ( type == "address-range" ) {
        start=getPVal(ln,"start_address")
        end=getPVal(ln,"end_address")
        ranges[id]=start";"end
    } else if ( type == "dnsname" ) {
        address=getPVal(ln,"dnsrec" )
        cmd="dig +short -t A "address" | tail -1"
        address=cmd | getline
        type="host"
        addresses[id]=address
    } else {
        address=getPVal(ln,"address")
        netmask=getPVal(ln,"netmask")

        addresses[id]=address
        netmasks[id]=netmask
    }
    names[id]=address
    comments[id]=comment
    obj[id]=type
    mark(id,name,address)
}

# Get Property Value (RHS)
function getPVal(item, prop) {
    prop=substr(item,index(item," "prop"=\"")+length(prop)+3)
    prop=substr(prop,1,index(prop,"\"")-1)
    return prop
}

function mark(id,name,address) {
    pat="[( ]"address"[) ]"
    if ( name ~ pat)
        marks[id]="ADDRESS IN NAME"
    else if ( name ~ /[^0-9a-zA-Z.-]/ )
        marks[id]="BAD CHARS"
    else if ( name == address )
        marks[id]="NAME EQUALS ADDRESS"
}

# MAIN BLOCK
{
    if ( $0 ~ /Library.*name="User-1"/ ) {
        while ( $0 !~ /<\/Library>/ ) {
            getline
            if ( $0 ~ /ObjectGroup.*name="Objects"/ )
                depth=1
            else if ( $0 ~ /<ObjectGroup.+[^\/]>/ && depth >= 1 ) {
                depth+=1
                if ( depth > 2 ) {
                    parse($0,"group")
                }
            } else if ( $0 ~ /<Network / && depth >= 2 )
                parse($0,"network")
            else if ( $0 ~ /<IPv4 / && depth >= 2 )
                parse($0,"host")
            else if ( $0 ~ /<AddressRange / && depth >= 2 )
                parse($0,"address-range")
            else if ( $0 ~ /<ObjectRef / && depth >= 2 ) {
                ref=getPVal($0,"ref")
                members[lastgroup]=members[lastgroup] ? members[lastgroup]";"ref : ref
            } else if ( $0 ~ /<DNSName / )
                parse($0,"dnsname")
            else if ( $0 ~ /<\/ObjectGroup>/ )
                depth-=1
        }
    }
}

# Generate CSV
END {
    print("id|type|name|comment|address|netmask|first;last|members|marker")
    for ( id in obj)
        printf("%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
            id,
            obj[id],
            names[id],
            comments[id],
            addresses[id],
            netmasks[id],
            ranges[id],
            members[id],
            marks[id])
    for ( id in groups )
        printf("%s|group|%s|%s|%s|%s|%s|%s|%s\n",
            id,
            names[id],
            comments[id],
            addresses[id],
            netmasks[id],
            ranges[id],
            members[id],
            marks[id])
}
